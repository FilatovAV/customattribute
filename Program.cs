﻿using System;
using System.Reflection;

namespace CustomAttribute
{
    class Program
    {
        static void Main(string[] args)
        {
            var remark = (typeof(TestAttribute).GetCustomAttribute(typeof(RemarkAttribute)) as RemarkAttribute).Remark;
            Console.ReadKey();
        }
    }
    [AttributeUsage(AttributeTargets.All)]
    public class RemarkAttribute : Attribute
    {
        string _remark;

        public RemarkAttribute(string comment)
        {
            _remark = comment;
        }

        public string Remark
        {
            get { return _remark; }
        }
    }
    [RemarkAttribute("My custom commentary.")]
    public class TestAttribute
    {
        public string SValue { get; set; }
    }
}
